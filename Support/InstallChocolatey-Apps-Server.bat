REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin

call cinst disableuac

REM Apps
call cinst filezilla
call cinst 7zip
call cinst console2
call cinst GoogleChrome
call cinst git
call cinst tortoisegit
call cinst SourceTree
call cinst notepadplusplus
call cinst xplorer2
call cinst procexp
call cinst westwindwebsurge
call cinst deletefiles

call cinst filezilla.server

REM call choco install mssql2014express-defaultinstance
REM call choco install mssqlservermanagementstudio2014express