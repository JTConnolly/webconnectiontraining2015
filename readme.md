# Web Connection Training 2015 @ SouthWest Fox
### Materials, Notes and Support Materials

> ##### Under Construction
> The materials here are not complete yet, but will be regularly updated, before during and after the conference. You can clone this repro and keep up to date.

Thank you for attending the Training event at Southwest Fox. This repository contains all the Slides and documents for the sessions at the conference. This years conference is a bit different than others in that it's mostly about code, so there are less support materials than usual.

But we also provide a host of features like Visual Studio Intellisense Snippets, FoxCode Intellisense snippets, and links to separate source code repositories for the two main sample applications we will build and talk about at the conference.

### Online Samples
The following are the fully built sample applications we worked on:

* [Trakker Server Sample](http://TimeTrakkerswf.west-wind.com/Time)
* [AlbumViewer Client Side Mobile Sample](http://albumviewerswf.west-wind.com/)

### Sample Source Code
The source code for the completed samples that we'll be working on during the conference are available at:

* [TimeTrakker Server Side Sample App](https://bitbucket.org/RickStrahl/southwestfoxtimetrakker)
* [AlbumViewer Client Side Sample App](https://bitbucket.org/RickStrahl/southwestfoxalbumviewer)

### Updates
I will be updating this example **before, during and after** the conference, so it's probably a good idea to use Git to clone this repository.

### Downloading this Content
I recommend that you clone this Git repository to your local machine as I will be updating it frequently. By cloning the repository you can simply pull updates to your machine incrementally as they occur.

You can download materials in any of the BitBucket repositories by clicking the Downloads button in the left section of the repo. You can switch branches and access specific tags to downloads specific versions/branches as needed. We'll be using branches for the code we work on during the conference.

### Git Clients
If you want to get the latest versions of materials at all times including during the conference, I advise to use Git to keep the repro synced up rather than downloading as it's much quicker and only pulls what has changed.

You can use the Git command line but personally I like:

* [TortoiseGit](https://tortoisegit.org/)  (for Windows Shell Integration in Explorer)
* [SourceTree](https://www.sourcetreeapp.com/)   (a nice and free Git UI) 

